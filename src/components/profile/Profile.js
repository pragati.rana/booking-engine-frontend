import React from "react";
import styles from "./styles/profileStyles";
import {Button} from "@material-ui/core";

function Profile(){

    const classes = styles();

    return (
        <>
            <div className={classes.headerLink}>
                <h1>
                    User Profile
                </h1>
            </div>
            <div className={classes.username}>
                <p>Username</p>
                <p>seed-user-1</p>
            </div>
            <Button 
                variant="contained"
                type="submit"
                color="primary"
                className={classes.changePassButton}>
                Change Password
            </Button> 
        </>
    );
}

export default Profile;