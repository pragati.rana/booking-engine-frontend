import {makeStyles} from "@material-ui/core";

export default makeStyles((theme) => ({
        headerLink: {
            display: 'flex',
            margin: "20px",
        },

        username: {
            display: 'flex-row',
            justifyContent: "flex-start",
            margin: "20px",
            fontSize: "20px",
        },

        changePassButton: {
            display: "flex",
            margin: "15px",
        },
    })
);
